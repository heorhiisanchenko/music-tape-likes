import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {ConfigModule} from '@nestjs/config'
import {TypeOrmModule} from '@nestjs/typeorm';
import {LikesModule} from './likes/likes.module';
import {AuthModule} from './auth/auth.module';
import {SharedModule} from './shared/shared.module';

@Module({
    imports: [
        ConfigModule.forRoot({isGlobal: true}),
        TypeOrmModule.forRoot({
            type: 'mysql',
            host:  process.env.DB_HOST,
            port: parseInt(process.env.DB_PORT),
            username: process.env.DB_USER,
            password:  process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
            autoLoadEntities: true,
            synchronize: true,
        }),
        LikesModule,
        AuthModule,
        SharedModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
