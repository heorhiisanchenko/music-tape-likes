import * as dotenv from 'dotenv';
import {resolve} from 'path';
dotenv.config({path: resolve(__dirname, '../dev.env')});
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
// console.log({
//   host:  process.env.DB_HOST,
//   port: parseInt(process.env.DB_PORT),
//   username: process.env.DB_USER,
//   password:  process.env.DB_PASSWORD,
//   database: process.env.DB_NAME,
// })

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(process.env.APP_PORT || 3000);
}
bootstrap();
